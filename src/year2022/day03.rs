use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::{
    collections::{HashMap, HashSet},
    num::ParseIntError,
};
use thiserror::Error;

#[aoc_generator(day3)]
fn parse_input(input: &str) -> Result<Vec<String>, ParseError> {
    Ok(input.lines().map(str::to_string).collect())
}

#[derive(Error, Debug)]
enum ParseError {
    #[error("Couldn't parse the number!")]
    Number(#[from] ParseIntError),
    #[error("Infallible!")]
    Infallible(#[from] std::convert::Infallible),
}

#[aoc(day3, part1)]
fn part1(rucksacks: &[String]) -> usize {
    let mut to_sum = Vec::new();
    'rl: for rucksack in rucksacks {
        let count = rucksack.len() / 2;
        let mut hs = HashSet::new();

        for item in rucksack.chars().take(count) {
            hs.insert(item);
        }

        for item in rucksack.chars().skip(count) {
            if hs.contains(&item) == true {
                let num = if item.is_lowercase() { 96 } else { 38 };
                to_sum.push(item as usize - num);
                //to_sum.push(item as usize %	32);
                continue 'rl;
            }
        }
    }
    to_sum.iter().sum()
}

#[aoc(day3, part2)]
fn part2(rucksacks: &[String]) -> usize {
    let mut to_sum = Vec::new();
    for (r1, r2, r3) in rucksacks.iter().tuples() {
        let mut hs: HashMap<char, usize> = HashMap::new();
        for item in r1
            .chars()
            .unique()
            .chain(r2.chars().unique())
            .chain(r3.chars().unique())
        {
            hs.entry(item).and_modify(|f| *f += 1).or_default();
        }
        let key = hs.iter().max_by_key(|v| v.1).unwrap().0;
        let num = if key.is_lowercase() { 96 } else { 38 };
        to_sum.push(*key as usize - num);
    }
    to_sum.iter().sum()
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;

    static DATA: Lazy<Vec<String>> =
        Lazy::new(|| parse_input(include_str!("../../input/2022/day3.txt")).unwrap());
    static SAMPLE_DATA: Lazy<Vec<String>> =
        Lazy::new(|| parse_input(include_str!("../../input/2022/day3sample.txt")).unwrap());

    #[test]
    fn part1_test() {
        assert_eq!(part1(&DATA), 7990);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&SAMPLE_DATA), 157);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&DATA), 2602);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&SAMPLE_DATA), 70);
    }
}
