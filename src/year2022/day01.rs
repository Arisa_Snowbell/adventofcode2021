use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::num::ParseIntError;
use thiserror::Error;

#[aoc_generator(day1)]
fn parse_input(input: &str) -> Result<Vec<Vec<usize>>, ParseError> {
    Ok(input
        .split("\n\n")
        .map(|s| {
            s.lines()
                .map(|x| x.parse::<usize>().unwrap())
                .collect::<Vec<usize>>()
        })
        .collect())
}

#[derive(Error, Debug)]
enum ParseError {
    #[error("Couldn't parse the number!")]
    Number(#[from] ParseIntError),
    #[error("Infallible!")]
    Infallible(#[from] std::convert::Infallible),
}

#[aoc(day1, part1)]
fn part1(items: &Vec<Vec<usize>>) -> usize {
    *items
        .iter()
        .map(|s| s.iter().sum())
        .collect::<Vec<usize>>()
        .iter()
        .max()
        .unwrap()
}

#[aoc(day1, part2)]
fn part2(items: &Vec<Vec<usize>>) -> usize {
    items
        .iter()
        .map(|s| s.iter().sum())
        .collect::<Vec<usize>>()
        .iter()
        .sorted()
        .rev()
        .take(3)
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;

    static DATA: Lazy<Vec<Vec<usize>>> =
        Lazy::new(|| parse_input(include_str!("../../input/2022/day1.txt")).unwrap());
    static SAMPLE_DATA: Lazy<Vec<Vec<usize>>> =
        Lazy::new(|| parse_input(include_str!("../../input/2022/day1sample.txt")).unwrap());

    #[test]
    fn part1_test() {
        assert_eq!(part1(&DATA), 70_764);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&SAMPLE_DATA), 24_000);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&DATA), 203_905);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&SAMPLE_DATA), 45_000);
    }
}
