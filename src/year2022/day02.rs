use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::{num::ParseIntError, ops::Not};
use thiserror::Error;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Hand {
    Rock = 1,
    Paper = 2,
    Scissor = 3,
}

impl Not for Hand {
    type Output = Hand;

    fn not(self) -> Self::Output {
        match self {
            Hand::Rock => Hand::Paper,
            Hand::Paper => Hand::Scissor,
            Hand::Scissor => Hand::Rock,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Winner {
    Enemy(Hand, Hand),
    Me(Hand, Hand),
    Draw(Hand),
}

impl TryFrom<(Hand, char)> for Winner {
    type Error = ParseError;

    fn try_from(value: (Hand, char)) -> Result<Self, Self::Error> {
        let (h, c) = value;
        match c {
            'X' => Ok(Winner::Enemy(!!h, h)), // Lose
            'Y' => Ok(Winner::Draw(h)),       // Draw
            'Z' => Ok(Winner::Me(h, !h)),     // Win
            _ => Err(ParseError::WrongWinnerChar(c)),
        }
    }
}

impl Winner {
    fn score(&self) -> (usize, usize) {
        let mut enemy_score = 0;
        let mut me_score = 0;
        match self {
            Winner::Enemy(enemy, me) => {
                enemy_score += *enemy as usize + 6;
                me_score += *me as usize;
            }
            Winner::Me(enemy, me) => {
                me_score += *me as usize + 6;
                enemy_score += *enemy as usize;
            }
            Winner::Draw(i) => {
                let score = *i as usize + 3;
                enemy_score += score;
                me_score += score;
            }
        }

        (enemy_score, me_score)
    }
}

fn winner_is(hands: (Hand, Hand)) -> Winner {
    match hands {
        (Hand::Rock, Hand::Paper) => Winner::Me(hands.0, hands.1),
        (Hand::Paper, Hand::Rock) => Winner::Enemy(hands.0, hands.1),
        (Hand::Rock, Hand::Scissor) => Winner::Enemy(hands.0, hands.1),
        (Hand::Scissor, Hand::Rock) => Winner::Me(hands.0, hands.1),
        (Hand::Paper, Hand::Scissor) => Winner::Me(hands.0, hands.1),
        (Hand::Scissor, Hand::Paper) => Winner::Enemy(hands.0, hands.1),
        (Hand::Rock, Hand::Rock) => Winner::Draw(Hand::Rock),
        (Hand::Paper, Hand::Paper) => Winner::Draw(Hand::Paper),
        (Hand::Scissor, Hand::Scissor) => Winner::Draw(Hand::Scissor),
    }
}

impl TryFrom<char> for Hand {
    type Error = ParseError;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'A' | 'X' => Ok(Self::Rock),
            'B' | 'Y' => Ok(Self::Paper),
            'C' | 'Z' => Ok(Self::Scissor),
            i => Err(ParseError::WrongHandChar(i)),
        }
    }
}

#[aoc_generator(day2)]
fn parse_input(input: &str) -> Result<Vec<(Hand, (Hand, Hand))>, ParseError> {
    Ok(input
        .lines()
        .map(|s| {
            (
                s.chars().nth(0).unwrap().try_into().unwrap(),
                (
                    s.chars().nth(2).unwrap().try_into().unwrap(),
                    match (
                        s.chars().nth(0).unwrap().try_into().unwrap(),
                        s.chars().nth(2).unwrap(),
                    )
                        .try_into()
                        .unwrap()
                    {
                        Winner::Me(_, h2) => h2,
                        Winner::Enemy(h2, _) => h2,
                        Winner::Draw(h2) => h2,
                    },
                ),
            )
        })
        .collect())
}

#[derive(Error, Debug)]
enum ParseError {
    #[error("Couldn't parse the number!")]
    Number(#[from] ParseIntError),
    #[error("Infallible!")]
    Infallible(#[from] std::convert::Infallible),
    #[error("Incorrect char \'{0}\' for Hand!")]
    WrongHandChar(char),
    #[error("Incorrect char \'{0}\' for Winner!")]
    WrongWinnerChar(char),
}

#[aoc(day2, part1)]
fn part1(items: &Vec<(Hand, (Hand, Hand))>) -> usize {
    items
        .iter()
        .map(|(h1, (h2, _))| winner_is((*h1, *h2)).score().1)
        .sum()
}

#[aoc(day2, part2)]
fn part2(items: &Vec<(Hand, (Hand, Hand))>) -> usize {
    items
        .iter()
        .map(|(h1, (_, h2))| winner_is((*h1, *h2)).score().1)
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;

    static DATA: Lazy<Vec<(Hand, (Hand, Hand))>> =
        Lazy::new(|| parse_input(include_str!("../../input/2022/day2.txt")).unwrap());
    static SAMPLE_DATA: Lazy<Vec<(Hand, (Hand, Hand))>> =
        Lazy::new(|| parse_input(include_str!("../../input/2022/day2sample.txt")).unwrap());

    #[test]
    fn part1_test() {
        assert_eq!(part1(&DATA), 13809);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&SAMPLE_DATA), 15);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&DATA), 12316);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&SAMPLE_DATA), 12);
    }
}
