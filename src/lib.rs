#![feature(lazy_cell)]
#![feature(slice_group_by)]
#![warn(clippy::all, clippy::nursery)]

use aoc_runner_derive::aoc_lib;

// mod year2015;
// aoc_lib! { year = 2015 }

// mod year2017;
// aoc_lib! { year = 2016 }

// mod year2017;
// aoc_lib! { year = 2017 }

// mod year2018;
// aoc_lib! { year = 2018 }

// mod year2019;
// aoc_lib! { year = 2019 }

// mod year2020;
// aoc_lib! { year = 2020 }

// mod year2021;
// aoc_lib! { year = 2021 }

// mod year2022;
// aoc_lib! { year = 2022 }

mod year2023;
aoc_lib! { year = 2023 }
