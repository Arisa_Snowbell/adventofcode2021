use std::{num::ParseIntError, str::FromStr};

use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day7)]
fn parse_input(input: &str) -> Result<Vec<usize>, ParseIntError> {
    input
        .split(',')
        .map(str::trim)
        .map(FromStr::from_str)
        .collect()
}

#[aoc(day7, part1)]
fn part1(crabs: &[usize]) -> usize {
    let mut min_fuel = None;
    for position in 0..=*crabs.iter().max().unwrap() {
        let curr_fuel = crabs.iter().fold(0, |accu, &crab| {
            let min = crab.min(position);
            let max = crab.max(position);
            accu + (max - min)
        });
        match min_fuel {
            Some(prev_fuel) => {
                if prev_fuel > curr_fuel {
                    min_fuel = Some(curr_fuel);
                }
            }
            None => min_fuel = Some(curr_fuel),
        }
    }

    min_fuel.unwrap()
}

#[aoc(day7, part2)]
fn part2(crabs: &[usize]) -> usize {
    let mut min_fuel = None;
    for position in 0..=*crabs.iter().max().unwrap() {
        let curr_fuel = crabs.iter().fold(0, |accu, &crab| {
            let min = crab.min(position);
            let max = crab.max(position);
            accu + (1..=max - min).sum::<usize>()
        });
        match min_fuel {
            Some(prev_fuel) => {
                if prev_fuel > curr_fuel {
                    min_fuel = Some(curr_fuel);
                }
            }
            None => min_fuel = Some(curr_fuel),
        }
    }

    min_fuel.unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    const DATA: &str = include_str!("../../input/2021/day7.txt");
    const SAMPLE_DATA: &str = include_str!("../../input/2021/day7sample.txt");

    #[test]
    fn part1_test() {
        assert_eq!(part1(&parse_input(DATA).unwrap()), 342730);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&parse_input(SAMPLE_DATA).unwrap()), 37);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&parse_input(DATA).unwrap()), 92335207);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&parse_input(SAMPLE_DATA).unwrap()), 168);
    }
}
