use aoc_runner_derive::{aoc, aoc_generator};
use std::{num::ParseIntError, str::FromStr};
use thiserror::Error;

#[aoc_generator(day2)]
fn parse_input(input: &str) -> Result<Vec<Direction>, ParseError> {
    input.lines().map(Direction::from_str).collect()
}

#[derive(Error, Debug)]
enum ParseError {
    #[error("Couldn't parse direction!")]
    Direction,
    #[error("Couldn't parse number!")]
    ParseIntError(#[from] ParseIntError),
}

enum Direction {
    Forward(isize),
    Down(isize),
    Up(isize),
}

impl FromStr for Direction {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.split_once(' ') {
            Some(("forward", value)) => Ok(Self::Forward(value.parse()?)),
            Some(("down", value)) => Ok(Self::Down(value.parse()?)),
            Some(("up", value)) => Ok(Self::Up(value.parse()?)),
            _ => Err(Self::Err::Direction),
        }
    }
}

#[aoc(day2, part1)]
fn part1(directions: &[Direction]) -> isize {
    let mut horizontal = 0;
    let mut depth = 0;

    for direction in directions {
        match direction {
            Direction::Forward(value) => horizontal += value,
            Direction::Down(value) => depth += value,
            Direction::Up(value) => depth -= value,
        }
    }

    depth * horizontal
}

#[aoc(day2, part2)]
fn part2(directions: &[Direction]) -> isize {
    let mut horizontal = 0;
    let mut depth = 0;
    let mut aim = 0;

    for direction in directions {
        match direction {
            Direction::Forward(value) => {
                horizontal += value;
                depth += aim * value;
            }
            Direction::Down(value) => aim += value,
            Direction::Up(value) => aim -= value,
        }
    }

    depth * horizontal
}

#[cfg(test)]
mod tests {
    use super::*;

    const DATA: &str = include_str!("../../input/2021/day2.txt");
    const SAMPLE_DATA: &str = include_str!("../../input/2021/day2sample.txt");

    #[test]
    fn part1_test() {
        assert_eq!(part1(&parse_input(DATA).unwrap()), 1250395);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&parse_input(SAMPLE_DATA).unwrap()), 150);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&parse_input(DATA).unwrap()), 1451210346);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&parse_input(SAMPLE_DATA).unwrap()), 900);
    }
}
