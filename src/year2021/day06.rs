use std::{num::ParseIntError, str::FromStr};

use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day6)]
fn parse_input(input: &str) -> Result<Vec<usize>, ParseIntError> {
    input
        .split(',')
        .map(str::trim)
        .map(FromStr::from_str)
        .collect()
}

fn count_fish(input: &[usize], days: usize) -> usize {
    let mut school = [0; 9];
    input.iter().for_each(|&fish| school[fish] += 1);

    (0..days).for_each(|_| {
        school.rotate_left(1);
        school[6] += school[8];
    });

    school.iter().sum()
}

#[aoc(day6, part1)]
fn part1(fishes: &[usize]) -> usize {
    /*
    let mut prev_day: Vec<(u8, bool)>  = fishes.iter().map(|x| (*x, false)).collect();
    for _ in 0..80 {
        let mut today = prev_day.clone();
        let mut count = 0;
        for (fish, birth) in today.iter_mut() {
            if *fish == 0 {
                *fish = 6;
                *birth = true;
                count+=1;
            }
        }
        for (fish, birth) in today.iter_mut() {
            if *fish > 0 && !*birth {
                *fish -= 1;
            }
            if *birth {
                *birth = false;
            }
        }
        today.resize(today.len()+count, (8, false));


         prev_day = today;
     }

     prev_day.len()
    */
    // YES YES I JUMPED ON THIS, It hurt, a lot, I am lazy and dumb

    count_fish(fishes, 80)
}

#[aoc(day6, part2)]
fn part2(fishes: &[usize]) -> usize {
    count_fish(fishes, 256)
}

#[cfg(test)]
mod tests {
    use super::*;

    const DATA: &str = include_str!("../../input/2021/day6.txt");
    const SAMPLE_DATA: &str = include_str!("../../input/2021/day6sample.txt");

    #[test]
    fn part1_test() {
        assert_eq!(part1(&parse_input(DATA).unwrap()), 350149);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&parse_input(SAMPLE_DATA).unwrap()), 5934);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&parse_input(DATA).unwrap()), 1590327954513);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&parse_input(SAMPLE_DATA).unwrap()), 26984457539);
    }
}
