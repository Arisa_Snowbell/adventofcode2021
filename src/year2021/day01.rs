use aoc_runner_derive::{aoc, aoc_generator};
use std::{num::ParseIntError, str::FromStr};

#[aoc_generator(day1)]
fn parse_input(input: &str) -> Result<Vec<usize>, ParseIntError> {
    input.lines().map(FromStr::from_str).collect()
}

#[aoc(day1, part1)]
fn part1(depths: &[usize]) -> usize {
    depths.windows(2).filter(|win| win[1] > win[0]).count()
}

#[aoc(day1, part2)]
fn part2(depths: &[usize]) -> usize {
    depths.windows(4).filter(|win| win[3] > win[0]).count()
}

#[cfg(test)]
mod tests {
    use super::*;

    const DATA: &str = include_str!("../../input/2021/day1.txt");
    const SAMPLE_DATA: &str = include_str!("../../input/2021/day1sample.txt");

    #[test]
    fn part1_test() {
        assert_eq!(part1(&parse_input(DATA).unwrap()), 1624);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&parse_input(SAMPLE_DATA).unwrap()), 7);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&parse_input(DATA).unwrap()), 1653);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&parse_input(SAMPLE_DATA).unwrap()), 5);
    }
}
