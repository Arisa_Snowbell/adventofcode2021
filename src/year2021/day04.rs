use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::{num::ParseIntError, ops::Mul, str::FromStr};
use thiserror::Error;

#[aoc_generator(day4)]
fn parse_input(input: &str) -> Result<(Vec<u8>, Vec<Board>), ParseError> {
    let mut lines = input.lines();
    let drawn_nums = lines
        .next()
        .expect("Couldn't get first line!")
        .split(',')
        .map(FromStr::from_str)
        .filter_map(Result::ok) // FIXME: UNWRAP, this basically ignores errors when parsing, that's not good
        .collect();

    let boards = lines
        .chunks(6)
        .into_iter()
        .skip(1)
        .map(|mut lines| lines.join("\n")[1..].parse())
        .filter_map(Result::ok) // FIXME: UNWRAP, this basically ignores errors when parsing, that's not good
        .collect();

    Ok((drawn_nums, boards))
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Cell {
    Marked,
    Value(u8),
}

impl FromStr for Cell {
    type Err = ParseError;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        Ok(Cell::Value(string.trim().parse()?))
    }
}

#[derive(Debug, Clone)]
struct Board {
    board: [[Cell; 5]; 5],
    win_num: Option<u8>,
}

#[derive(Error, Debug)]
enum ParseError {
    #[error("Couldn't parse the board!")]
    Board(#[from] ParseIntError),
}

impl FromStr for Board {
    type Err = ParseError;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        let mut board: [[Cell; 5]; 5] = [[Cell::Value(0); 5]; 5];

        string.lines().enumerate().for_each(|(col, line)| {
            line.chars()
                .chunks(3)
                .into_iter()
                .map(|chars| chars.collect::<String>())
                .map(|x| x.parse())
                .filter_map(Result::ok) // FIXME: UNWRAP, this basically ignores errors when parsing, that's not good
                .enumerate()
                .for_each(|(row, cell)| board[col][row] = cell)
        });

        Ok(Board {
            board,
            win_num: None,
        })
    }
}

impl Board {
    fn mark_spot(&mut self, num: u8) {
        self.board.iter_mut().flatten().for_each(|cell| {
            if *cell == Cell::Value(num) {
                *cell = Cell::Marked;
            }
        });
    }

    fn is_winning(&self) -> bool {
        let row_win = self
            .board
            .iter()
            .any(|row| row.iter().all(|&cell| cell == Cell::Marked));

        let column_win = self
            .board
            .iter()
            .enumerate()
            .any(|(s, _)| self.board.iter().all(|row| row[s] == Cell::Marked));

        row_win || column_win
    }

    fn get_score(&self) -> Option<usize> {
        let win_num = self.win_num?;

        let score = self
            .board
            .iter()
            .flatten()
            .fold(0usize, |accu, cell| match cell {
                Cell::Marked => accu,
                Cell::Value(num) => accu + *num as usize,
            })
            .mul(win_num as usize);

        Some(score)
    }
}

#[aoc(day4, part1)]
fn part1((drawn_nums, boards): &(Vec<u8>, Vec<Board>)) -> usize {
    let mut boards = boards.to_vec();
    let mut winning_boards = Vec::new();

    for num in drawn_nums {
        for board in boards.iter_mut() {
            board.mark_spot(*num);
            if board.is_winning() {
                board.win_num = Some(*num);
                winning_boards.push(board.clone());
                break;
            }
        }
    }

    winning_boards.first().unwrap().get_score().unwrap()
}

#[aoc(day4, part2)]
fn part2((drawn_nums, boards): &(Vec<u8>, Vec<Board>)) -> usize {
    let mut boards = boards.to_vec();
    let mut winning_boards = Vec::new();

    for num in drawn_nums {
        for board in boards.iter_mut() {
            if board.win_num.is_none() {
                board.mark_spot(*num);
                if board.is_winning() {
                    board.win_num = Some(*num);
                    winning_boards.push(board.clone());
                }
            }
        }
    }

    winning_boards.last().unwrap().get_score().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    const DATA: &str = include_str!("../../input/2021/day4.txt");
    const SAMPLE_DATA: &str = include_str!("../../input/2021/day4sample.txt");

    #[test]
    fn part1_test() {
        assert_eq!(part1(&parse_input(DATA).unwrap()), 6592);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&parse_input(SAMPLE_DATA).unwrap()), 4512);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&parse_input(DATA).unwrap()), 31755);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&parse_input(SAMPLE_DATA).unwrap()), 1924);
    }
}
