use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::{
    collections::{HashMap, HashSet},
    error::Error,
    num::ParseIntError,
    str::FromStr,
};
use thiserror::Error;

#[aoc_generator(day7)]
fn parse_input(input: &str) -> Result<_, ParseError> {
    Ok(_)
}

#[derive(Error, Debug)]
enum ParseError {
    #[error("Couldn't parse the number!")]
    Number(#[from] ParseIntError),
    #[error("Invalid bag!")]
    InvalidBag,
    #[error("Invalid baggy!")]
    InvalidBaggy,
    #[error("Infallible!")]
    Infallible(#[from] std::convert::Infallible),
}

#[aoc(day7, part1)]
fn part1(items: &_) -> usize {
    1
}

#[aoc(day7, part2)]
fn part2(items: &_) -> usize {
    1
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;

    static DATA: Lazy<_> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day7.txt")).unwrap());
    static SAMPLE_DATA: Lazy<_> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day7sample.txt")).unwrap());

    #[test]
    fn part1_test() {
        assert_eq!(part1(&DATA), 0);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&SAMPLE_DATA), 0);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&DATA), 0);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&SAMPLE_DATA), 0);
    }
}
