use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::{error::Error, num::ParseIntError, str::FromStr};
use thiserror::Error;

#[aoc_generator(day8)]
fn parse_input(input: &str) -> Result<Vec<Instruction>, ParseError> {
    Ok(input
        .lines()
        .map(|int| Instruction::from_str(int).unwrap())
        .collect())
}

#[derive(Debug, Clone, Copy)]
enum Instruction {
    Nop(isize),
    Jmp(isize),
    Acc(isize),
}

impl FromStr for Instruction {
    type Err = ParseError;

    fn from_str(instruction: &str) -> Result<Self, Self::Err> {
        let Some((toi, ip)) = instruction.split_once(" ") else {
			return Err(ParseError::NotProperFormat);
		};
        let ip = ip.parse()?;
        Ok(match toi {
            "acc" => Self::Acc(ip),
            "jmp" => Self::Jmp(ip),
            "nop" => Self::Nop(ip),
            _ => return Err(ParseError::InvalidInstruction),
        })
    }
}

#[derive(Error, Debug)]
enum ParseError {
    #[error("Couldn't parse the number!")]
    Number(#[from] ParseIntError),
    #[error("Invalid instruction!")]
    InvalidInstruction,
    #[error("Invalid format!")]
    NotProperFormat,
    #[error("Infallible!")]
    Infallible(#[from] std::convert::Infallible),
}

#[derive(Debug, Default)]
struct VM {
    //instructions: Vec<Instruction>,
    executed: Vec<usize>,
    accumulator: isize,
    cursor: isize,
    infinite_loop: bool,
}

fn run(instructions: &Vec<Instruction>) -> VM {
    let VM {
        mut executed,
        mut accumulator,
        mut cursor,
        infinite_loop: _,
    } = VM::default();
    while !executed.contains(&(cursor as usize)) {
        executed.push(cursor as usize);
        //println!("C: {cursor}");
        match instructions.get(cursor as usize) {
            Some(Instruction::Jmp(ip)) => cursor += *ip - 1,
            Some(Instruction::Nop(_)) => {}
            Some(Instruction::Acc(ip)) => accumulator += *ip,
            None => {
                return VM {
                    executed,
                    accumulator,
                    cursor,
                    infinite_loop: false,
                }
            }
        }
        cursor += 1;
    }

    VM {
        executed,
        accumulator,
        cursor,
        infinite_loop: true,
    }
}

#[aoc(day8, part1)]
fn part1(instructions: &Vec<Instruction>) -> isize {
    let VM {
        executed: _,
        accumulator,
        cursor: _,
        infinite_loop: _,
    } = run(instructions);
    accumulator
}

#[aoc(day8, part2)]
fn part2(instructions: &Vec<Instruction>) -> isize {
    let VM {
        executed,
        accumulator: _,
        cursor: _,
        infinite_loop: _,
    } = run(instructions);
    let mut instructions = instructions.clone();
    for executed in executed.iter().rev() {
        match instructions[*executed] {
            Instruction::Jmp(ip) => {
                instructions[*executed] = Instruction::Nop(ip);
                if let VM {
                    executed: _,
                    accumulator,
                    cursor: _,
                    infinite_loop: false,
                } = run(&instructions)
                {
                    return accumulator;
                }
                instructions[*executed] = Instruction::Jmp(ip);
            }
            Instruction::Nop(ip) => {
                instructions[*executed] = Instruction::Jmp(ip);
                if let VM {
                    executed: _,
                    accumulator,
                    cursor: _,
                    infinite_loop: false,
                } = run(&instructions)
                {
                    return accumulator;
                }
                instructions[*executed] = Instruction::Nop(ip);
            }
            _ => continue,
        }
    }
    panic!("No solution")
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;

    static DATA: Lazy<Vec<Instruction>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day8.txt")).unwrap());
    static SAMPLE_DATA: Lazy<Vec<Instruction>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day8sample.txt")).unwrap());

    #[test]
    fn part1_test() {
        assert_eq!(part1(&DATA), 2058);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&SAMPLE_DATA), 5);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&DATA), 1000);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&SAMPLE_DATA), 8);
    }
}
