use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::{error::Error, num::ParseIntError, str::FromStr};
use thiserror::Error;

#[aoc_generator(day9)]
fn parse_input(input: &str) -> Result<Vec<usize>, ParseError> {
    Ok(input.lines().map(|s| s.parse().unwrap()).collect())
}

#[derive(Error, Debug)]
enum ParseError {
    #[error("Couldn't parse the number!")]
    Number(#[from] ParseIntError),
    #[error("Infallible!")]
    Infallible(#[from] std::convert::Infallible),
}

fn find_weakness(numbers: &Vec<usize>, preamble_length: usize) -> Option<usize> {
    for (index, key) in numbers.iter().enumerate().skip(preamble_length) {
        let from_index = if (index as isize - preamble_length as isize) >= 0 {
            index - preamble_length
        } else {
            0
        };
        //println!("index: {index}, from_index: {from_index}");
        //println!("{:?}", &items[from_index..index]);
        let preamble: Vec<usize> = numbers[from_index..index]
            .iter()
            .combinations(2)
            .map(|a| a.iter().copied().sum())
            .chain(numbers[from_index..index].iter().copied())
            .collect();
        //println!("{preamble:?}");
        if !preamble.contains(key) {
            return Some(*key);
        }
    }
    None
}

#[aoc(day9, part1)]
fn part1(numbers: &Vec<usize>) -> usize {
    find_weakness(numbers, 25).unwrap()
}

fn crack_xmas(numbers: &Vec<usize>, preamble_length: usize) -> Option<usize> {
    let weakness = find_weakness(numbers, preamble_length).unwrap();

    for i in 0..numbers.len() {
        let mut rolling_sum = numbers[i];
        let mut high = numbers[i];
        let mut low = numbers[i];
        for n in 1..numbers.len() {
            let current = numbers[i + n];
            rolling_sum += current;
            if high < current {
                high = current;
            }

            if low > current {
                low = current;
            }

            if rolling_sum == weakness {
                return Some(low + high);
            }

            if rolling_sum > weakness {
                break;
            }
        }
    }

    None

    // My quickly made slow solution...
    // let mut to_sum = Vec::new();
    // for len in 1..numbers.len() {
    //     for item in numbers.windows(len) {
    //         let sum: usize = item.iter().sum();
    //         if sum == weakness {
    //             to_sum = item.to_vec();
    //             break;
    //         }
    //     }
    // }

    //to_sum.iter().min().unwrap() + to_sum.iter().max().unwrap()
}

#[aoc(day9, part2)]
fn part2(items: &Vec<usize>) -> usize {
    crack_xmas(items, 25).unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;

    static DATA: Lazy<Vec<usize>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day9.txt")).unwrap());
    static SAMPLE_DATA: Lazy<Vec<usize>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day9sample.txt")).unwrap());

    #[test]
    fn part1_test() {
        assert_eq!(part1(&DATA), 756008079);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(find_weakness(&SAMPLE_DATA, 5).unwrap(), 127);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&DATA), 93727241);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(crack_xmas(&SAMPLE_DATA, 5).unwrap(), 62);
    }
}
