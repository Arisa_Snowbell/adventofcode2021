use aoc_runner_derive::{aoc, aoc_generator};
use std::num::ParseIntError;

#[aoc_generator(day3)]
fn parse_input(input: &str) -> Result<Vec<Vec<bool>>, ParseIntError> {
    Ok(input
        .lines()
        .map(|line| {
            line.chars()
                .map(|c| c == '#')
                .collect()
        })
        .collect())
}

#[aoc(day3, part1)]
fn part1(lines: &[Vec<bool>]) -> usize {
    let mut position = 0;
    // let mut trees = 0;
    lines.iter().fold(0, |acc, f| {
        if f.iter().cycle().nth(position) == Some(&true) {
            position += 3;
            acc + 1
        } else {
            position += 3;
            acc
        }
    })
    // for line in lines.iter() {
    //     let mut inf_line = line.chars().cycle();
    //     if inf_line.nth(position) == Some('#') {
    //         trees += 1;
    //     }
    //     position += 3;
    // }
    // trees
}

#[aoc(day3, part2)]
fn part2(lines: &[Vec<bool>]) -> usize {
    let mut trees = Vec::new();
    for (dx, dy) in &[(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)] {
        let mut x = 0;
        let mut tree = 0;
        for line in lines.iter().step_by(*dy) {
            let mut inf_line = line.iter().cycle();
            if inf_line.nth(x) == Some(&true) {
                tree += 1;
            }
            x += dx;
        }
        trees.push(tree);
        // match lines.get(y) {
        //     Some(line) => {
        //         let mut inf_line = line.chars().cycle();
        //         if inf_line.nth(x) == Some('#') {
        //             trees += 1;
        //         }
        //         x += dx;
        //         y += dy;
        //     }
        //     None => break,
        // }
    }
    // for line in lines.iter() {
    // 	let mut inf_line = line.chars().cycle();
    // 	if inf_line.nth(position) == Some('#') {
    // 		trees+=1;
    // 	}
    // 	position+=3;
    // }
    // println!("{trees:?}");
    trees.iter().product()
    // trees.iter().skip(1).fold(trees[0], |acc, f| acc * f)
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;

    static DATA: Lazy<Vec<Vec<bool>>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day3.txt")).unwrap());
    static SAMPLE_DATA: Lazy<Vec<Vec<bool>>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day3sample.txt")).unwrap());

    #[test]
    fn part1_test() {
        assert_eq!(part1(&DATA), 184);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&SAMPLE_DATA), 7);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&DATA), 2431272960);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&SAMPLE_DATA), 336);
    }
}
