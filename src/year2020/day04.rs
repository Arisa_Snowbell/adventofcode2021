use aoc_runner_derive::{aoc, aoc_generator};
use once_cell::sync::Lazy;
use regex::Regex;
use std::{num::ParseIntError, str::FromStr};
use thiserror::Error;

static HEX_COLOR_MATCH: Lazy<Regex> = Lazy::new(|| Regex::new(r"^#[0-9a-f]{6}$").unwrap());
static PID_MATCH: Lazy<Regex> = Lazy::new(|| Regex::new(r"^[0-9]{9}$").unwrap());

#[derive(Error, Debug)]
enum ParseError {
    #[error("Couldn't parse the number!")]
    Number(#[from] ParseIntError),
    #[error("Couldn't parse the MeasurementType!")]
    MeasurementType,
    //#[error("Couldn't parse the EyeColor!")]
    //EyeColor,
    #[error("Found invalid field!")]
    InvalidField,
    #[error("Infallible!")]
    Infallible(#[from] std::convert::Infallible),
}
#[derive(Debug, PartialEq, Eq)]
enum EyeColor {
    Amber,
    Blue,
    Brown,
    Grey,
    Green,
    Hazel,
    Other,
    Unknown,
}

impl FromStr for EyeColor {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "amb" => Self::Amber,
            "blu" => Self::Blue,
            "brn" => Self::Brown,
            "gry" => Self::Grey,
            "grn" => Self::Green,
            "hzl" => Self::Hazel,
            "oth" => Self::Other,
            _ => Self::Unknown, // This had to be added because part1 just wants to know if the field is there
                                // _ => return Err(ParseError::EyeColor),
        })
    }
}

#[derive(Debug)]
enum MeasurementType {
    CM,
    IN,
}

impl FromStr for MeasurementType {
    type Err = ParseError;

    fn from_str(mtype: &str) -> Result<Self, Self::Err> {
        match mtype.to_lowercase().as_str() {
            "cm" => Ok(Self::CM),
            "in" => Ok(Self::IN),
            _ => Err(ParseError::MeasurementType),
        }
    }
}

#[derive(Debug)]
struct Height {
    mtype: Option<MeasurementType>, // It has to be Option because in the real input there is one Height with only numbers and no measuremnt type
    value: usize,
}

impl FromStr for Height {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
		//let regex = Regex::new(r"^\d{4}-\a{2}-\d{2}$").unwrap();
        let mut se = String::new();
        let mut number = String::new();
        for c in s.chars() {
            if c.is_ascii_digit() {
                number.push(c);
            } else {
                se.push(c);
            }
        }
        Ok(Self {
            mtype: MeasurementType::from_str(&se).ok(),
            value: number.parse()?,
        })
    }
}
#[derive(Debug)]
struct Passport {
    byr: Option<usize>,
    iyr: Option<usize>,
    eyr: Option<usize>,
    hgt: Option<Height>,
    hcl: Option<String>,
    ecl: Option<EyeColor>,
    pid: Option<String>,
    cid: Option<String>,
}

impl FromStr for Passport {
    type Err = ParseError;

    fn from_str(item: &str) -> Result<Self, Self::Err> {
        let mut pp = Self {
            byr: None,
            iyr: None,
            eyr: None,
            hgt: None,
            hcl: None,
            ecl: None,
            pid: None,
            cid: None,
        };
        for line in item.lines() {
            for (name, value) in line.split_whitespace().filter_map(|f| f.split_once(':')) {
                //println!("{}:{}", field.0, field.1);
                match name {
                    "byr" => pp.byr = Some(value.parse()?),
                    "iyr" => pp.iyr = Some(value.parse()?),
                    "eyr" => pp.eyr = Some(value.parse()?),
                    "hgt" => pp.hgt = Some(value.parse()?),
                    "hcl" => pp.hcl = Some(value.to_string()),
                    "ecl" => pp.ecl = value.parse().ok(),
                    "pid" => pp.pid = Some(value.parse()?),
                    "cid" => pp.cid = Some(value.parse()?),
                    _ => return Err(ParseError::InvalidField),
                }
            }
        }
        Ok(pp)
    }
}

#[aoc_generator(day4)]
fn parse_input(input: &str) -> Result<Vec<Passport>, ParseError> {
    Ok(input
        .split("\n\n")
        .map(|l| Passport::from_str(l).unwrap())
        .collect())
}

#[aoc(day4, part1)]
fn part1(passports: &[Passport]) -> usize {
    let mut k = 0;
    for passport in passports {
        // println!("{passport:?}");
        if passport.pid.is_some()
            && passport.ecl.is_some()
            && passport.hcl.is_some()
            && passport.hgt.is_some()
            && passport.eyr.is_some()
            && passport.iyr.is_some()
            && passport.byr.is_some()
        {
            k += 1;
        }
    }
    k
}

#[aoc(day4, part2)]
fn part2(passports: &[Passport]) -> usize {
    let mut counter = 0;
    for passport in passports {
        let byr_valid = passport.byr >= Some(1920) && passport.byr <= Some(2002);
        let iyr_valid = passport.iyr >= Some(2010) && passport.iyr <= Some(2020);
        let eyr_valid = passport.eyr >= Some(2020) && passport.eyr <= Some(2030);
        let hgt_valid = passport.hgt.as_ref().map_or(false, |hgt| match hgt.mtype {
            Some(MeasurementType::CM) => hgt.value >= 150 && hgt.value <= 193,
            Some(MeasurementType::IN) => hgt.value >= 59 && hgt.value <= 76,
            None => false,
        });
		let hcl_valid = passport.hcl.as_ref().map_or(false, |hcl| HEX_COLOR_MATCH.is_match(hcl));
        //let hcl_valid = passport.hcl.as_ref().map_or(false, |hcl| {
        //	hcl.starts_with('#')
        //      && hcl.len() == 7
        //       && hcl[1..7].chars().all(|ch| ch.is_alphanumeric())
        //});
        if byr_valid
            && iyr_valid
            && eyr_valid
            && hgt_valid
            && hcl_valid
            && passport
                .ecl
                .as_ref()
                .is_some_and(|f| *f != EyeColor::Unknown)
            && passport.ecl.is_some()
			&& passport.pid.as_ref().is_some_and(|pid| PID_MATCH.is_match(pid))
            //&& passport
            //    .pid
            //    .as_ref()
            //    .is_some_and(|f| f.len() == 9 && f.chars().all(|f| f.is_ascii_digit()))
        {
            counter += 1;
        }
    }

    counter
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;

    static DATA: Lazy<Vec<Passport>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day4.txt")).unwrap());
    static SAMPLE_DATA: Lazy<Vec<Passport>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day4sample.txt")).unwrap());

    #[test]
    fn part1_test() {
        assert_eq!(part1(&DATA), 247);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&SAMPLE_DATA), 2);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&DATA), 145);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&SAMPLE_DATA), 2);
    }
}
