use aoc_runner_derive::{aoc, aoc_generator};
use std::{fmt::Display, num::ParseIntError, str::FromStr};

struct PasswordPolicyItem {
    min: usize,
    max: usize,
    ch: char,
    password: String,
}

impl FromStr for PasswordPolicyItem {
    type Err = Box<dyn std::error::Error>;

    fn from_str(line: &str) -> Result<Self, Self::Err> {
        let (rule, password) = line.split_once(':').unwrap();
        let (min_max, ch) = rule.split_once(' ').unwrap();
        let (min, max) = min_max.split_once('-').unwrap();
        Ok(Self {
            min: usize::from_str(min)?,
            max: usize::from_str(max)?,
            ch: ch.chars().last().unwrap(),
            password: password.trim().to_string(),
        })
    }
}

impl Display for PasswordPolicyItem {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}-{} {}: {}",
            self.min, self.max, self.ch, self.password
        )
    }
}

#[aoc_generator(day2)]
fn parse_input(input: &str) -> Result<Vec<PasswordPolicyItem>, ParseIntError> {
    Ok(input
        .lines()
        .flat_map(PasswordPolicyItem::from_str)
        .collect())
}

#[aoc(day2, part1)]
fn part1(items: &[PasswordPolicyItem]) -> usize {
    items
        .iter()
        .filter(|item| {
            let len = item.password.chars().filter(|&c| item.ch == c).count();
            len >= item.min && len <= item.max
        })
        .count()
}

#[aoc(day2, part2)]
fn part2(items: &[PasswordPolicyItem]) -> usize {
    // items
    //     .iter()
    //     .filter(|item| {
    //         let is_valid = item.password.chars().nth(item.min - 1) == Some(item.chara)
    //             && item.password.chars().nth(item.max - 1) == Some(item.chara);
    //         println!("{item} - {is_valid}");
    //         is_valid
    //     })
    //     .count()
    items
        .iter()
        .filter(
            |PasswordPolicyItem {
                 min,
                 max,
                 ch,
                 password,
             }| {
                (password.chars().nth(min - 1) == Some(*ch))
                    != (password.chars().nth(max - 1) == Some(*ch))
            },
        )
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;

    static DATA: Lazy<Vec<PasswordPolicyItem>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day2.txt")).unwrap());
    static SAMPLE_DATA: Lazy<Vec<PasswordPolicyItem>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day2sample.txt")).unwrap());

    #[test]
    fn part1_test() {
        assert_eq!(part1(&DATA), 536);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&SAMPLE_DATA), 2);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&DATA), 558);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&SAMPLE_DATA), 1);
    }
}
