use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::{
    collections::{HashMap, HashSet},
    num::ParseIntError,
};

#[aoc_generator(day6)]
fn parse_input(input: &str) -> Result<Vec<String>, ParseIntError> {
    Ok(input.split("\n\n").map(str::to_string).collect())
}

#[aoc(day6, part1)]
fn part1(answers: &Vec<String>) -> usize {
    //answers.iter().map(|group| HashSet::<char>::from_iter(group.chars()).len()).sum()
    // with itetools
    //answers.iter().map(|group| group.chars().unique().count()).sum()

    answers
        .iter()
        .map(|group| {
            group
                .chars()
                .filter(|ch| !ch.is_whitespace())
                .sorted()
                .dedup()
                .count()
        })
        .sum()
}

#[aoc(day6, part2)]
fn part2(answers: &Vec<String>) -> usize {
    //let mut all = Vec::new();
    answers
        .iter()
        .map(|group| {
            group
                .chars()
                .filter(|ch| !ch.is_whitespace())
                .unique()
                .filter(|f| group.lines().all(|l| l.contains(*f)))
                .count()
        })
        .sum()
    //for group in answers {
    //    let ans: Vec<char> = group
    //        //.replace("\n", "")
    //        .chars()
    //        .filter(|ch| !ch.is_whitespace())
    //        .unique()
    //        .collect();
    //    all.push(
    //        ans.into_iter()
    //            .filter(|f| group.lines().all(|l| l.contains(*f)))
    //            .count(),
    //    );
    //for person in group.lines() {
    //    //println!("{person}")
    //    'dsd: loop {
    //        for (i, ch) in ans.clone().into_iter().enumerate() {
    //            //println!("({i}, {ch})");
    //            if !person.contains(ch) {
    //                dbg!(ans.remove(i));
    //                continue 'dsd;
    //            }
    //        }
    //        break;
    //    }
    //}
    //println!("S: {kk:?}");
    //j.push(kk);
    //}

    //all.iter().sum()
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;

    static DATA: Lazy<Vec<String>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day6.txt")).unwrap());
    static SAMPLE_DATA: Lazy<Vec<String>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day6sample.txt")).unwrap());

    #[test]
    fn part1_test() {
        assert_eq!(part1(&DATA), 6565);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&SAMPLE_DATA), 11);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&DATA), 3137);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&SAMPLE_DATA), 6);
    }
}
