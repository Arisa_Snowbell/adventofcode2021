use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::{
    collections::{HashMap, HashSet},
    error::Error,
    num::ParseIntError,
    str::FromStr,
};
use thiserror::Error;

#[aoc_generator(day7)]
fn parse_input(input: &str) -> Result<HashMap<String, HashMap<String, usize>>, ParseError> {
    let bag_rules: Vec<BagRule> = input
        .lines()
        .filter(|s| !s.is_empty())
        .map(|br| BagRule::from_str(br).unwrap())
        .collect();

    let hai: HashMap<String, HashMap<String, usize>> = HashMap::from_iter(
        bag_rules
            .into_iter()
            .map(|br| (br.bag_umbrella, br.contains)),
    );

    Ok(hai)
}

#[derive(Error, Debug)]
enum ParseError {
    #[error("Couldn't parse the number!")]
    Number(#[from] ParseIntError),
    #[error("Invalid bag!")]
    InvalidBag,
    #[error("Invalid baggy!")]
    InvalidBaggy,
    #[error("Infallible!")]
    Infallible(#[from] std::convert::Infallible),
}

#[derive(Debug)]
struct BagRule {
    bag_umbrella: String,
    contains: HashMap<String, usize>,
}

impl FromStr for BagRule {
    type Err = ParseError;

    fn from_str(bag_rule: &str) -> Result<Self, Self::Err> {
        let Some((bag_umbrella, bags)) = bag_rule.split_once(" bags contain ") else {
			//eprintln!("LL: |{bag_rule}|");
			return Err(ParseError::InvalidBag);
		};
        let bag_umbrella = bag_umbrella.to_string();
        let mut contains = HashMap::new();
        if bags != "no other bags." {
            for baggy in bags.split(", ") {
                let baggy = baggy
                    .replace(".", "")
                    .replace(" bags", "")
                    .replace(" bag", "");
                //eprintln!("{bag_umbrella}:{baggy}");
                let Some((nob, tob)) = baggy.split_once(' ') else {
					//eprintln!("{baggy}");
					return Err(ParseError::InvalidBaggy)
				};
                contains.insert(tob.to_string(), nob.parse()?);
            }
        }

        Ok(BagRule {
            bag_umbrella,
            contains,
        })
    }
}

fn part1_proc(
    bag_type_to_find: &str,
    bag_rules: &HashMap<String, HashMap<String, usize>>,
    bags_found: &mut HashSet<String>,
) {
    for (bag, contents) in bag_rules {
        if contents.contains_key(bag_type_to_find) {
            bags_found.insert(bag.to_string());
            part1_proc(bag, bag_rules, bags_found)
        }
    }
}

#[aoc(day7, part1)]
fn part1(bag_rules: &HashMap<String, HashMap<String, usize>>) -> usize {
    //println!("{bag_rules:#?}");
    let mut bags_found = HashSet::new();
    part1_proc("shiny gold", bag_rules, &mut bags_found);
    bags_found.len()
}

fn part2_proc(
    bag_type_to_find: &str,
    bag_rules: &HashMap<String, HashMap<String, usize>>,
) -> usize {
    let mut total = 0;
    let contents = bag_rules.get(bag_type_to_find).unwrap();
    for (bag, count) in contents {
        let subcount = part2_proc(bag, bag_rules);
        if subcount > 0 {
            total += count * subcount;
        }
        total += count;
    }
    total
}

#[aoc(day7, part2)]
fn part2(bag_rules: &HashMap<String, HashMap<String, usize>>) -> usize {
    part2_proc("shiny gold", bag_rules)
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;

    static DATA: Lazy<HashMap<String, HashMap<String, usize>>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day7.txt")).unwrap());
    static SAMPLE_DATA: Lazy<HashMap<String, HashMap<String, usize>>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day7sample.txt")).unwrap());

    #[test]
    fn part1_test() {
        assert_eq!(part1(&DATA), 213);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&SAMPLE_DATA), 4);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&DATA), 38426);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&SAMPLE_DATA), 32);
    }
}
