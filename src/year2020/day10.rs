use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::{collections::HashMap, num::ParseIntError};
use thiserror::Error;

#[aoc_generator(day10)]
fn parse_input(input: &str) -> Result<Vec<usize>, ParseError> {
    Ok(input.lines().map(|l| l.trim().parse().unwrap()).collect())
}

#[derive(Error, Debug)]
enum ParseError {
    #[error("Couldn't parse the number!")]
    Number(#[from] ParseIntError),
    #[error("Infallible!")]
    Infallible(#[from] std::convert::Infallible),
}

#[aoc(day10, part1)]
fn part1(jolts: &Vec<usize>) -> usize {
    let device_rated_for_jolts: usize = jolts.iter().max().unwrap() + 3;
    let mut effective_charging_outlet = 0usize;
	let mut diff: Vec<usize> = vec![0; 3];

    for adapter in jolts.into_iter().sorted() {
        diff[adapter - effective_charging_outlet - 1] += 1;
        effective_charging_outlet = *adapter;
    }
    assert_eq!(device_rated_for_jolts, effective_charging_outlet + 3);

    dbg!(diff[0]) * dbg!(diff[2]+1)
}

#[aoc(day10, part2)]
fn part2(jolts: &Vec<usize>) -> usize {
    let mut magic: HashMap<usize, Vec<usize>> = HashMap::new();
    let device_rated_for_jolts: usize = *jolts.iter().max().unwrap();
	let mut jolts: Vec<usize> = jolts.into_iter().sorted().copied().collect();
	jolts.push(0);
	jolts.push(device_rated_for_jolts+3);
	jolts.sort();
    for (adapter_index, adapter) in jolts.iter().enumerate() {
        let mut possible_choices = Vec::new();

        for i in jolts[adapter_index+1..].iter() {
            if i - adapter <= 3 {
                possible_choices.push(*i);
            }
        }

        magic.insert(*adapter, possible_choices);
    }


	println!("{magic:?}");
    let mut unique = 0;
    loop {
        let mut possible_choices: Vec<Vec<usize>> = vec![magic.get(&0).unwrap().to_vec()];
        for possible_choice in possible_choices {
            //possible_choices.push(magic.get(&possible_choice).unwrap().to_vec());
        }
		break;
    }

    1
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;

    static DATA: Lazy<Vec<usize>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day10.txt")).unwrap());
    static SAMPLE_DATA: Lazy<Vec<usize>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day10sample.txt")).unwrap());

    #[test]
    fn part1_test() {
        assert_eq!(part1(&DATA), 1904);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&SAMPLE_DATA), 220);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&DATA), 0);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&SAMPLE_DATA), 0);
    }
}
