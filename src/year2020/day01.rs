use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::{num::ParseIntError, str::FromStr};

#[aoc_generator(day1)]
fn parse_input(input: &str) -> Result<Vec<usize>, ParseIntError> {
    input.lines().map(FromStr::from_str).collect()
}

#[aoc(day1, part1)]
fn part1(entries: &[usize]) -> usize {
    //    entries
    //       .iter()
    //      .combinations(2)
    //     .find(|a| a.iter().copied().sum::<usize>() == 2020)
    //    .unwrap()
    //   .into_iter()
    //  .product()
    //(&entries)
    //    .windows(2)
    //    .into_iter()
    //    .find(|n| n.first().zip(n.last()).map(|(a, b)| a + b) == Some(2020))
    //    .expect("SS")
    //    .iter()
    //    .product()
    let (a, b) = entries
        .iter()
        .tuple_combinations()
        .find(|(&a, &b)| a + b == 2020)
        .unwrap();
    a * b
}

#[aoc(day1, part2)]
fn part2(entries: &[usize]) -> usize {
    // entries
    //    .iter()
    //   .combinations(3)
    //  .find(|a| a.iter().copied().sum::<usize>() == 2020)
    // .unwrap()
    //.into_iter()
    //.product()
    let (a, b, c) = entries
        .iter()
        .tuple_combinations()
        .find(|(&a, &b, &c)| a + b + c == 2020)
        .unwrap();
    a * b * c
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;

    static DATA: Lazy<Vec<usize>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day1.txt")).unwrap());
    static SAMPLE_DATA: Lazy<Vec<usize>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day1sample.txt")).unwrap());

    #[test]
    fn part1_test() {
        assert_eq!(part1(&DATA), 964875);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&SAMPLE_DATA), 514579);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&DATA), 158661360);
    }

    #[test]
    fn part2_sample_test() {
        assert_eq!(part2(&SAMPLE_DATA), 241861950);
    }
}
