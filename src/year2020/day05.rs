use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::num::ParseIntError;

#[aoc_generator(day5)]
fn parse_input(input: &str) -> Result<Vec<String>, ParseIntError> {
    Ok(input.lines().map(str::to_string).collect())
}

fn decode_seat(encoded_seat: &str) -> usize {
    let w_row = &encoded_seat[..7];
    let w_column = &encoded_seat[7..];
    //    let mut row = 0;
    //    for (i, ch) in w_row.chars().rev().enumerate() {
    //        if ch == 'B' {
    //            row += 1 << i;
    //            println!("{i} -> {row}");
    //        }
    //    }
    //    let mut column = 0;
    //    for (i, ch) in w_column.chars().rev().enumerate() {
    //        if ch == 'R' {
    //            column += 1 << i;
    //        }
    //    }
    let row = binary_find(w_row, 'F', 'B', 128_f64);
    let column = binary_find(w_column, 'L', 'R', 8_f64);
    (row * 8) + column
}

fn binary_find(line: &str, bottom: char, _top: char, hi: f64) -> usize {
    let mut hi = hi;
    let mut lo = 0_f64;
    for ch in line.chars() {
        let mid = ((lo + hi) / 2_f64).floor();
        if ch == bottom {
            hi = mid;
        } else if ch == _top {
            lo = mid;
        }
    }
    lo as usize
}

#[aoc(day5, part1)]
fn part1(encoded_seats: &[String]) -> usize {
    let seats: Vec<_> = encoded_seats
        .into_iter()
        .map(|en_seat| decode_seat(en_seat))
        .collect();
    *seats.iter().max().unwrap() // Or sort and get last element
}

#[aoc(day5, part2)]
fn part2(encoded_seats: &[String]) -> usize {
    let seats: Vec<_> = encoded_seats
        .into_iter()
        .map(|en_seat| decode_seat(en_seat))
        .sorted()
        .collect();

    // let s = seats.windows(2).find(|n| n[0] == n[1] - 2).unwrap()[0] + 1;

    //for (en0, en1) in seats.iter().tuple_windows() {
    //    if (en1 - en0) > 1 {
    //        println!("{}:{}", en0, en1);
    //        return en0 + 1;
    //    }
    //}

    let theoretical_sum: usize = (seats[0]..=seats[seats.len() - 1]).sum();
    let real_sum: usize = seats.iter().sum();
    theoretical_sum - real_sum
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;

    static DATA: Lazy<Vec<String>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day5.txt")).unwrap());
    static SAMPLE_DATA: Lazy<Vec<String>> =
        Lazy::new(|| parse_input(include_str!("../../input/2020/day5sample.txt")).unwrap());

    #[test]
    fn part1_test() {
        assert_eq!(part1(&DATA), 976);
    }

    #[test]
    fn part1_sample_test() {
        assert_eq!(part1(&SAMPLE_DATA), 820);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2(&DATA), 685);
    }

    // NOTE: This test had to be disabled as the example input is not applicable to the real input for part2
    //#[test]
    //fn part2_sample_test() {
    //    assert_eq!(part2(&SAMPLE_DATA), 120);
    //}
}
